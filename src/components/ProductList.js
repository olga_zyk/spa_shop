import React, {Component} from 'react';
import {Card, CardBody, CardImg, CardSubtitle, CardTitle, Col, Row} from 'reactstrap';


class ProductList extends Component {
  static getUserCurrency() {
    return {id: 1, name: 'Euro', code: 'EUR', render: '€'};
  }

  render() {
    const currency = ProductList.getUserCurrency();
    const items = this.props.list.map(item =>
      <Col key={item.id} xs={12} sm={6} md={6} lg={3}>
        <Card>
          <CardImg top width="100%" src={item.img} alt="slippers_photo"/>
          <CardBody>
            <CardTitle>{item.name}</CardTitle>
            <CardSubtitle>{`${item.price} ${currency.render}`}</CardSubtitle>
          </CardBody>
        </Card>
      </Col>
    );

    return (
      <Row>
        {items}
      </Row>
    );
  }
}

export default ProductList;