import React, {Component} from 'react';
import {Container} from 'reactstrap';
import NavBar from './NavBar';

import '../styles/App.css';

class App extends Component {


  render() {
    return (
      <Container>
        <NavBar/>
        <div className="content">
          {this.props.children}
        </div>
      </Container>
    );
  }
}

export default App;
