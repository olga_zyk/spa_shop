import React, {Component} from 'react';
import {Button, Container, Input, InputGroup, Row, Table} from 'reactstrap';
import TableFilter from 'react-table-filter';
import SampleOrders from '../sampleData/orders.json';


class OrdersPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: OrdersPage.getOrdersList()
    };

    this.filterUpdated = this.filterUpdated.bind(this);

  }

  static getOrdersList() {
    const ordersList = SampleOrders;

    return ordersList;

  }

  filterUpdated(newData) {
    this.setState({
      data: newData
    });
  }

  render() {
    const data = this.state.data;
    const rows = data.map(row =>
      <tr>
        <td className="cell">{row.firstName}</td>
        <td className="cell">{row.lastName}</td>
        <td className="cell">{row.orderName}</td>
        <td className="cell">{row.size}</td>
      </tr>
    );

    return (
      <Container>
        <div className="ordersList"><h1>List of Orders</h1></div>
        <Row>
          <InputGroup>
            <Input type="text" placeholder="Search"/>
            <Button color="success" type="submit">Search</Button>
          </InputGroup>
        </Row>

        <Row>
          <Table bordered>
            <TableFilter rows={data} onFilterUpdate={this.filterUpdated}>
              <th filterkey="firstName" className="cell" showSearch={true}>First Name</th>
              <th filterkey="lastName" className="cell">Last Name</th>
              <th filterkey="orderName" className="cell">Order Name</th>
              <th filterkey="size" className="cell">Size</th>
            </TableFilter>
            <tbody>
            {rows}
            </tbody>
          </Table>
        </Row>
      </Container>
    );
  }
}

export default OrdersPage;