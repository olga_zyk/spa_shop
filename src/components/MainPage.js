import React, {Component} from 'react';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  Label,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row
} from 'reactstrap';
import ProductList from './ProductList';
import SampleProducts from '../sampleData/products.json';

class MainPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: MainPage.getItemList(),     //set initial State
      search: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.makeSearch = this.makeSearch.bind(this);
  }


  static getItemList() {
    const initialItemList = SampleProducts;

    return initialItemList;
  }

  handleChange(event) {
    this.setState({
      search: event.target.value
    });
  }

  makeSearch(event) {
    const regex = new RegExp(`${this.state.search}`, 'i');
    const searchResult = MainPage.getItemList().filter(product => regex.test(product.name) || this.state.search.trim().length === 0);
    this.setState({
      list: searchResult
    });


    event.preventDefault();
  }

  render() {
    return (
      <div>
        <Row>
          <Col>
            <Form onSubmit={this.makeSearch}>
              <InputGroup>
                <Input type="text" value={this.state.search} onChange={this.handleChange} placeholder="Search"/>
                <Button color="success" type="submit">Search</Button>
              </InputGroup>
            </Form>
          </Col>
        </Row>

        <Row>
          <Col>
            <Form>
              <Row>
                <Col md={3}>
                  <FormGroup>
                    <Label for="colorFilter">Price</Label>
                    <Input multiple type="select" name="colors" id="colorFilter">
                      <option>1-10</option>
                      <option>11-20</option>
                      <option>21-30</option>
                      <option>31-40</option>
                      <option>41+</option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <FormGroup>
                    <Label for="colorFilter">Color</Label>
                    <Input multiple type="select" name="colors" id="colorFilter">
                      <option>Red</option>
                      <option>Green</option>
                      <option>Yellow</option>
                      <option>Pink</option>
                      <option>Grey</option>
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>


        <Row>
          <Pagination>
            <PaginationItem>
              <PaginationLink previous href="#"/>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">
                1
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">
                2
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">
                3
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">
                4
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">
                5
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink next href="#"/>
            </PaginationItem>
          </Pagination>
        </Row>

        <Row>
          <ProductList list={this.state.list}/>
        </Row>
      </div>

    );
  }
}

export default MainPage;