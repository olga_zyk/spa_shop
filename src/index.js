import React from 'react';
import ReactDOM from 'react-dom';
import {hashHistory, IndexRoute, Route, Router} from 'react-router';
import './styles/index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-table-filter/lib/styles.css';
import App from './components/App';
import MainPage from './components/MainPage';
import OrdersPage from './components/OrdersPage';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={MainPage}/>
      <Route path="/orders" component={OrdersPage}/>
    </Route>
  </Router>,
  document.getElementById('root'));
registerServiceWorker();
